::::
:: @Package     : edk-beni
:: @FileName    : build.cmd
:: @Date        : 20230219
:: @Author      : Jiangwei
:: @Version     : 1.0
:: @Description :
::   This is used to build OVMF binary on Windows.
::   The tools below needed in order to build successfully:
::   1. Visual Studio Community 2015.
::   2. Nasm.
::   3. Asl.
::   4. Git.
::
:: @History:
::   20230219: Initialize.
::   20230225: Add script to download jansson for Redfish.
::
:: This program and the accompanying materials
:: are licensed and made available under the terms and conditions of the BSD License
:: which accompanies this distribution. The full text of the license may be found at
:: http://opensource.org/licenses/bsd-license.php
::
:: THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
:: WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
::::

@echo off

:: Directory set.
set CUR_DIR=%CD%
SET EDK_DIR=%CUR_DIR%\edk2
:: It seems a BUG exists, can't use a dir named edk2-staging or edk2staging.
SET EDK_STAGING_DIR=%CUR_DIR%\staging
SET OEM_DIR=%CUR_DIR%\beni
SET BASETOOL_BROTLI_DIR=%EDK_DIR%\BaseTools\Source\C\BrotliCompress\brotli
SET MDE_MODULE_BROTLI_DIR=%EDK_DIR%\MdeModulePkg\Library\BrotliCustomDecompressLib\brotli
SET ONIGURUMA_DIR=%EDK_DIR%\MdeModulePkg\Universal\RegularExpressionDxe\oniguruma
SET OPENSSL_DIR=%EDK_DIR%\CryptoPkg\Library\OpensslLib\openssl
SET JSON_DIR=%EDK_DIR%\RedfishPkg\Library\JsonLib\jansson
:: Here use the GenFv.exe to check if basetools exist.
SET BASETOOL_WIN_GEN_FV=%EDK_DIR%\BaseTools\Bin\Win32\GenFv.exe

:: Environment set.
set TOOLS=VS2015x86
set PYTHON_COMMAND=py -3
set PACKAGES_PATH=%OEM_DIR%;%EDK_STAGING_DIR%;%EDK_DIR%

:: Different edk2 tag depends on different submoule source code revision, make
:: sure to use the right revision. How to get the right revision:
:: 1. Checkout the edk2 tag, for example:
::      PS D:\Gitee\beni\edk2> git tag edk2-stable202211
:: 2. Check the submoudle, for example
::      PS D:\Gitee\beni\edk2> git submodule
::      -b64af41c3276f97f0e181920400ee056b9c88037 ArmPkg/Library/ArmSoftFloatLib/berkeley-softfloat-3
::      -f4153a09f87cbb9c826d8fc12c74642bb2d879ea BaseTools/Source/C/BrotliCompress/brotli
::      -d82e959e621a3d597f1e0d50ff8c2d8b96915fd7 CryptoPkg/Library/OpensslLib/openssl
::      -f4153a09f87cbb9c826d8fc12c74642bb2d879ea MdeModulePkg/Library/BrotliCustomDecompressLib/brotli
::      -abfc8ff81df4067f309032467785e06975678f0d MdeModulePkg/Universal/RegularExpressionDxe/oniguruma
::      -e9ebfa7e77a6bee77df44e096b100e7131044059 RedfishPkg/Library/JsonLib/jansson
::      -1cc9cde3448cdd2e000886a26acf1caac2db7cf1 UnitTestFrameworkPkg/Library/CmockaLib/cmocka
::      -86add13493e5c881d7e4ba77fb91c1f57752b3a4 UnitTestFrameworkPkg/Library/GoogleTestLib/googletest
:: 3. After clone the submodule, use the first 7 digit to checkout the related revision.
:: Here we use the following tag and relative revision.
SET EDK2_TAG=edk2-stable202211
SET BROTLI_REV=f4153a0
SET ONIGURUMA_REV=abfc8ff
SET OPENSSL_REV=d82e959
SET JSON_REV=e9ebfa7

:: Help statement.
if /I "%1"=="-h"        goto HELP
if /I "%1"=="--help"    goto HELP
if /I "%1"=="help"      goto HELP
if /I "%1"=="/?"        goto HELP

:: Clean workspace.
if /I "%1"=="clean"     goto CLEAN

:: Start QEMU.
if /I "%1"=="start"     goto START

:: Download edk2 source code.
if exist %EDK_DIR%\.git (
    echo ### edk2 exists, no need to git clone ...
) else (
    echo ### git clone edk2 ...
    git clone https://gitee.com/jiangwei0512/edk2.git %EDK_DIR%
    cd %EDK_DIR%
    echo ### checkout %EDK2_TAG% ...
    git checkout %EDK2_TAG%
    cd %CUR_DIR%
)
:: Use the RedfishPkg in edk2-staging, so delete this one.
if exist %EDK_DIR%\RedfishPkg (
    echo ### delte RedfishPkg in edk2 ...
    rd /S /Q %EDK_DIR%\RedfishPkg
)

:: Download brotli for BaseTools.
if exist %BASETOOL_BROTLI_DIR%\.git (
    echo ### BaseTools brotli exists, no need to git clone ...
) else (
    echo ### git clone brotli for BaseTools ...
    git clone https://gitee.com/jiangwei0512/brotli.git %BASETOOL_BROTLI_DIR%
    cd %BASETOOL_BROTLI_DIR%
    echo ### checkout %BROTLI_REV% ...
    git checkout %BROTLI_REV%
    cd %CUR_DIR%
)

:: Download brotli for MdeModulePkg.
if exist %MDE_MODULE_BROTLI_DIR%\.git (
    echo ### MdeModulePkg brotli exists, no need to git clone ...
) else (
    echo ### git clone brotli for MdeModulePkg ...
    git clone https://gitee.com/jiangwei0512/brotli.git %MDE_MODULE_BROTLI_DIR%
    cd %MDE_MODULE_BROTLI_DIR%
    echo ### checkout %BROTLI_REV% ...
    git checkout %BROTLI_REV%
    cd %CUR_DIR%
)

:: Download oniguruma.
if exist %ONIGURUMA_DIR%\.git (
    echo ### oniguruma exists, no need to git clone ...
) else (
    echo ### git clone oniguruma ...
    git clone https://gitee.com/jiangwei0512/oniguruma.git %ONIGURUMA_DIR%
    cd %ONIGURUMA_DIR%
    echo ### checkout %ONIGURUMA_REV% ...
    git checkout %ONIGURUMA_REV%
    cd %CUR_DIR%
)

:: Download openssl.
if exist %OPENSSL_DIR%\.git (
    echo ### openssl exists, no need to git clone ...
) else (
    echo ### git clone openssl ...
    git clone https://gitee.com/jiangwei0512/openssl.git %OPENSSL_DIR%
    cd %OPENSSL_DIR%
    echo ### checkout %OPENSSL_REV% ...
    git checkout %OPENSSL_REV%
    cd %CUR_DIR%
)

:: Download jansson.
:: Use the one in edk2-staging, no need to download twice.
@REM if exist %JSON_DIR%\.git (
@REM     echo ### json exists, no need to git clone ...
@REM ) else (
@REM     echo ### git clone json ...
@REM     git clone https://gitee.com/jiangwei0512/jansson.git %JSON_DIR%
@REM     cd %JSON_DIR%
@REM     echo ### checkout %JSON_REV% ...
@REM     git checkout %JSON_REV%
@REM     cd %CUR_DIR%
@REM )

cd %EDK_DIR%
:: Check BaseTools in Win32.
if exist %BASETOOL_WIN_GEN_FV% (
    echo ### no need to build basetools ...
) else (
    echo ### building basetools ...
    call edksetup.bat rebuild
)

if not exist %BASETOOL_WIN_GEN_FV% (
    echo ### basetools not exist ...
    goto DONE
)

:: All prepared, start to build something.
call edksetup.bat
if not "%1"=="" (
    if /I "%1"=="emulator" (
        if not "%2"=="" (
            if /I "%2"=="run" (
                goto EMULATOR
            ) else (
                goto HELP
            )
        )
        goto EMULATOR
    )
    if /I "%1"=="payload"   goto PAYLOAD
    if /I "%1"=="ovmf"      goto OVMF
    goto HELP
) else (
    :: Default build OVMF.
    goto OVMF
)

:EMULATOR
echo ### building Emulator ...
call build -p EmulatorPkg\EmulatorPkg.dsc -a X64 -t %TOOLS%
if %errorlevel%==0 (
    echo ### build result: SUCCESS!
    if /I "%2"=="run" (
        echo ### runing emulator...
        cd %EDK_DIR%\Build\EmulatorX64\DEBUG_%TOOLS%\X64\ && start WinHost.exe
    )
    goto DONE
) else (
    echo ### build result: FAILED!
    goto ERROR
)

:PAYLOAD
echo ### building Payload ...
call build -p UefiPayloadPkg\UefiPayloadPkg.dsc -a IA32 -a X64 -b DEBUG -t %TOOLS% -D BOOTLOADER=SBL
if %errorlevel%==0 (
    echo ### build result: SUCCESS!
    goto DONE
) else (
    echo ### build result: FAILED!
    goto ERROR
)

:OVMF
echo ### building OVMF ...
call edksetup.bat
call build -p BeniPkg/OvmfPkgX64.dsc -a X64 -a IA32 -t %TOOLS%
if %errorlevel%==0 (
    echo ### build result: SUCCESS!
    copy %EDK_DIR%\Build\OvmfX64\DEBUG_%TOOLS%\FV\OVMF.fd %CUR_DIR%\OVMF.fd
    goto DONE
) else (
    echo ### build result: FAILED!
    goto ERROR
)

:START
echo ### start QEMU ...
if "%2"=="" (
    qemu-system-x86_64 -machine q35,smm=on -drive if=pflash,format=raw,unit=0,file=%CUR_DIR%\OVMF.fd -serial stdio > log.txt
) else (
    if "%2"=="disk" (
        qemu-system-x86_64 -machine q35,smm=on -drive if=pflash,format=raw,unit=0,file=%CUR_DIR%\OVMF.fd -drive format=raw,file=%CUR_DIR%\disk.img -serial stdio > log.txt
    )
    if "%2"=="net" (
        qemu-system-x86_64 -machine q35,smm=on -drive if=pflash,format=raw,unit=0,file=%CUR_DIR%\OVMF.fd -net nic -net tap,ifname=tap0 -serial stdio > log.txt
    )
)
goto DONE

:CLEAN
echo ### cleaning ...
cd %EDK_DIR%
:: Delete directory Build.
if exist Build    rd /S /Q Build
if exist BuildFsp rd /S /Q BuildFsp
if exist *.log    del /Q *.log
:: Delete files and directories in Conf.
if exist Conf\.cache\.PlatformBuild     del /Q Conf\.cache\.PlatformBuild
if exist Conf\.AutoGenIdFile.txt        del /Q Conf\.AutoGenIdFile.txt
if exist Conf\build_rule.txt            del /Q Conf\build_rule.txt
if exist Conf\target.txt                del /Q Conf\target.txt
if exist Conf\tools_def.txt             del /Q Conf\tools_def.txt
:: Delete BIOS binary.
cd %CUR_DIR%
if exist log.txt    del /Q log.txt
if exist OVMF*.fd   del /Q OVMF*.fd
echo ### done!
goto DONE

:HELP
echo.
echo ############################################################################
echo #
echo # Usage:
echo #  Build.cmd [ovmf] [start [disk] [net]] [clean] [emulator [run]] [payload]
echo #
echo ############################################################################
goto DONE

:ERROR
echo ### oops!! You got some problems!
goto DONE

:DONE
cd %CUR_DIR%
echo on
@exit /b
