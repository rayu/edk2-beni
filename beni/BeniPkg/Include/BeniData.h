/**
*  @Package     : BeniPkg
*  @FileName    : BeniData.h
*  @Date        : 20211004
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This file describe the BENI GUID and related data.
*
*  @History:
*    20211004: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __BENI_DATA_H__
#define __BENI_DATA_H__

#include <Uefi.h>

#define BENI_DATA_NAME  L"BeniData"
#define BENI_DATA_MAGIC 0xA55A5AA5
#define BENI_DATA_SIZE  128

//
// {DB56E93F-C5EF-4888-8006-F64DBCBBF755}
//
#define BENI_DATA_GUID \
  { \
    0xdb56e93f, 0xc5ef, 0x4888, { 0x80, 0x06, 0xf6, 0x4d, 0xbc, 0xbb, 0xf7, 0x55 } \
  }
//
// {10C71E2C-49E5-4D01-AC3C-3BA18DC47D56}
//
#define BENI_HOB_GUID \
  { \
    0x10c71e2c, 0x49e5, 0x4d01, { 0xac, 0x3c, 0x3b, 0xa1, 0x8d, 0xc4, 0x7d, 0x56 } \
  }

#define BENI_HOB_DATA_LEN 16

typedef struct _BENI_HOB_DATA {
  UINT8 Data[BENI_HOB_DATA_LEN];
} BENI_HOB_DATA;

extern EFI_GUID gBeniGlobalDataGuid;
extern EFI_GUID gBeniHobGuid;

#endif // __BENI_DATA_H__
