/**
*  @Package     : BeniPkg
*  @FileName    : BeniDebugLib.h
*  @Date        : 20230319
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is a library for DEBUG.
*
*  @History:
*    20230319: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __BENI_DEBUG_LIB_H__
#define __BENI_DEBUG_LIB_H__

#include <Uefi.h>

/**
  Dump some hexadecimal data.

  @param[in]  Indent                How many spaces to indent the output.
  @param[in]  Offset                The offset of the printing.
  @param[in]  DataSize              The size in bytes of UserData.
  @param[in]  UserData              The data to print out.

  @retval  NA

**/
VOID
EFIAPI
BeniDumpHex (
  IN  UINTN                         Indent,
  IN  UINTN                         Offset,
  IN  UINTN                         DataSize,
  IN  VOID                          *UserData
  );

#endif // __BENI_DEBUG_LIB_H__
