/**
*  @Package     : BeniPkg
*  @FileName    : HiiLib.h
*  @Date        : 20230225
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is a library for assembly code test.
*
*  @History:
*    20230225: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#ifndef __BENI_HII_LIB_H__
#define __BENI_HII_LIB_H__

#include <Uefi.h>

#include <Protocol/HiiPackageList.h>

/**
  Retrieve HII package list from ImageHandle and publish to HII database.

  @param[in]  ImageHandle           The image handle of the process.

  @return  EFI_HII_HANDLE           The HII handle.

**/
EFI_HII_HANDLE
BeniInitializeHiiPackage (
  IN  EFI_HANDLE                    ImageHandle
  );

#endif // __BENI_HII_LIB_H__
