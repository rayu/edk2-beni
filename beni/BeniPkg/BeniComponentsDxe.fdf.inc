##
#  @Package     : BeniPkg
#  @FileName    : BeniComponentsDxe.fdf.inc
#  @Date        : 20230220
#  @Author      : Jiangwei
#  @Version     : 1.0
#  @Description :
#    This file contains components for learning BIOS.
#
#  @History:
#    20230220: Initialize.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

#
# Modules.
#
INF BeniPkg/Dxe/DxeDriverInBds/DxeDriverInBds.inf
INF BeniPkg/Dxe/EventTestDxe/EventTestDxe.inf
!if $(BENI_EXT4_SUPPORT) == TRUE
  INF BeniPkg/Dxe/Ext4Dxe/Ext4Dxe.inf
!else
  INF BeniPkg/Dxe/Ramdisk/Ramdisk.inf
!endif
INF BeniPkg/Dxe/GetAppFromFv/GetAppFromFv.inf
!if $(INTEL_NIC_SOURCE_CODE) == TRUE
  INF BeniPkg/Dxe/GigUndiDxe/GigUndiDxe.inf
!endif
INF BeniPkg/Dxe/GlobalDataInstall/GlobalDataInstall.inf
INF BeniPkg/Dxe/HelloWorldDxe/HelloWorldDxe.inf
INF BeniPkg/Dxe/MemoryAllocationTest/MemoryAllocationTest.inf
INF BeniPkg/Dxe/NullDxeDriverOne/NullDxeDriverOne.inf
INF BeniPkg/Dxe/NullDxeDriverTwo/NullDxeDriverTwo.inf
INF BeniPkg/Dxe/PcdTestDriver/PcdTestDriver.inf
INF BeniPkg/Dxe/ProtocolConsumer/ProtocolConsumer.inf
INF BeniPkg/Dxe/ProtocolServer/ProtocolServer.inf
INF BeniPkg/Dxe/HobConsumer/HobConsumer.inf

#
# EFI Redfish drivers.
#
!include RedfishPkg/Redfish.fdf.inc
!include RedfishClientPkg/RedfishClient.fdf.inc

#
# Shell commands.
#
INF BeniPkg/DynamicCommand/DiskDynamicCommand/DiskDynamicCommand.inf
INF BeniPkg/DynamicCommand/ExecuteShellAppCommand/ExecuteShellAppCommand.inf
INF BeniPkg/DynamicCommand/SetupDynamicCommand/SetupDynamicCommand.inf
INF BeniPkg/DynamicCommand/TestDynamicCommand/TestDynamicCommand.inf
INF BeniPkg/DynamicCommand/VarDynamicCommand/VarDynamicCommand.inf

#
# Applications and scripts in FV. Modify GetAppFromFv.c accordingly.
#
FILE FREEFORM = DF674789-4265-4056-9DD0-5123BB5A81F5 {
 SECTION RAW = $(OUTPUT_DIRECTORY)/$(COMPILE_DIR)/X64/BeniPkg/App/HelloWorldApp/HelloWorldApp/OUTPUT/helloworld.efi
}
FILE FREEFORM = 8F989B9A-3E27-4352-8A16-43DA1DBA7A85 {
  SECTION RAW = $(OUTPUT_DIRECTORY)/$(COMPILE_DIR)/X64/BeniPkg/App/AppDevPath/AppDevPath/OUTPUT/appdp.efi
}
FILE FREEFORM = F103E554-2509-4935-B779-33F571249762 {
  SECTION RAW = $(OUTPUT_DIRECTORY)/$(COMPILE_DIR)/X64/BeniPkg/App/MemTest/MemTest/OUTPUT/memtest.efi
}
FILE FREEFORM = 0CCB5D25-27EE-4099-8D08-009A33433CEF {
  SECTION RAW = $(OUTPUT_DIRECTORY)/$(COMPILE_DIR)/X64/BeniPkg/App/EventTestApp/EventTestApp/OUTPUT/event.efi
}
FILE FREEFORM = 5DCC1F61-FCF9-4158-9A98-DD41E2983954 {
  SECTION RAW = BeniPkg/Script/startup.nsh
}
FILE FREEFORM = 188B916C-0B41-406C-B434-8E1FED6CF751 {
  SECTION RAW = BeniPkg/Script/example.json
}
!if $(REDFISH_ENABLE) == TRUE
FILE FREEFORM = A7CC7EAF-8625-46B5-B0C4-17B17B5C417F {
  SECTION RAW = $(OUTPUT_DIRECTORY)/$(COMPILE_DIR)/X64/EmulatorPkg/Application/RedfishPlatformConfig/RedfishPlatformConfig/OUTPUT/RedfishPlatformConfig.efi
}
!endif
