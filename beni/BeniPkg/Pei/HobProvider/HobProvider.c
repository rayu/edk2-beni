/**
*  @Package     : BeniPkg
*  @FileName    : ProtocolServer.c
*  @Date        : 20211004
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    Install BENI_HELLO_WORLD_PROTOCOL.
*
*  @History:
*    20211004: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <PiPei.h>
#include <BeniData.h>

#include <Library/HobLib.h>
#include <Library/DebugLib.h>
#include <Library/PeimEntryPoint.h>

/**
  Main entry of PEIM.

  @param[in]  FileHandle            Handle of the file being invoked.
  @param[in]  PeiServices           Describes the list of possible PEI Services.

  @retval  EFI_SUCCESS              The PEIM initialized successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
HobProviderEntry (
  IN  EFI_PEI_FILE_HANDLE           FileHandle,
  IN  CONST EFI_PEI_SERVICES        **PeiServices
  )
{
  BENI_HOB_DATA *Hob = NULL;
  UINT8         Index = 0;

  DEBUG ((EFI_D_ERROR, "Creating HOB ...\n"));

  Hob = BuildGuidHob (&gBeniHobGuid, sizeof (BENI_HOB_DATA));
  if (NULL == Hob) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] BuildGuidHob failed.\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  for (Index = 0; Index < BENI_HOB_DATA_LEN; Index++) {
    Hob->Data[Index] = Index;
  }

  DEBUG ((EFI_D_ERROR, "Done\n"));

  return EFI_SUCCESS;
}
