/**
*  @Package     : BeniPkg
*  @FileName    : DebugInfoLib.c
*  @Date        : 20230319
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is a library for DEBUG.
*
*  @History:
*    20230319: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Library/DebugLib.h>
#include <Library/BeniDebugInfoLib.h>

STATIC CONST CHAR8 mHex[] = {
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F'
};

/**
  Dump some hexadecimal data.

  @param[in]  Indent                How many spaces to indent the output.
  @param[in]  Offset                The offset of the printing.
  @param[in]  DataSize              The size in bytes of UserData.
  @param[in]  UserData              The data to print out.

  @retval  NA

**/
VOID
EFIAPI
BeniDumpHex (
  IN  UINTN                         Indent,
  IN  UINTN                         Offset,
  IN  UINTN                         DataSize,
  IN  VOID                          *UserData
  )
{
  UINT8  *Data;
  UINT8  TempByte;
  UINTN  Size;
  UINTN  Index;
  CHAR8  Val[50];
  CHAR8  Str[20];

  Data = UserData;
  while (DataSize != 0) {
    Size = 16;
    if (Size > DataSize) {
      Size = DataSize;
    }

    for (Index = 0; Index < Size; Index += 1) {
      TempByte           = Data[Index];
      Val[Index * 3 + 0] = mHex[TempByte >> 4];
      Val[Index * 3 + 1] = mHex[TempByte & 0xF];
      Val[Index * 3 + 2] = (CHAR8)((Index == 7) ? '-' : ' ');
      Str[Index]         = (CHAR8)((TempByte < ' ' || TempByte > '~') ? '.' : TempByte);
    }

    Val[Index * 3] = 0;
    Str[Index]     = 0;
    DEBUG ((EFI_D_ERROR, "%*a%08X: %-48a *%a*\r\n", Indent, "", Offset, Val, Str));

    Data     += Size;
    Offset   += Size;
    DataSize -= Size;
  }
}
