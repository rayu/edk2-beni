/**
*  @Package     : BeniPkg
*  @FileName    : SupportLib.c
*  @Date        : 20220121
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is for memory test.
*
*  @History:
*    20220121: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

/** @file
  Memory test support library

  Copyright (c) 2009, Intel Corporation
  Copyright (c) 2009, Jordan Justen
  All rights reserved.

  This program and the accompanying materials are licensed and made
  available under the terms and conditions of the BSD License which
  accompanies this distribution.  The full text of the license may
  be found at http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS"
  BASIS, WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER
  EXPRESS OR IMPLIED.

**/

#include "SupportLib.h"

STATIC MEM_TEST_INSTANCE  *mTests = NULL;
STATIC UINTN              mTestCount = 0;
STATIC UINTN              mMaxTestCount = 0;
STATIC BOOLEAN            mAbortTesting = FALSE;

/**
  Run memory range test.

  @param[in]  Context               Test data.

  @retval  EFI_SUCCESS              Operation complete.
  @retval  Others                   Operation failed.

**/
STATIC
EFI_STATUS
EFIAPI
RunMemoryRangeTest (
  IN  VOID                          *Context
  )
{
  EFI_STATUS            Status = EFI_SUCCESS;
  UINTN                 Key = 0;
  MEM_RANGE_TEST_DATA   *Test = (MEM_RANGE_TEST_DATA*)Context;
  EFI_PHYSICAL_ADDRESS  Start = 0;
  UINT64                Length = 0;
  UINT64                LengthTested = 0;
  UINT64                LengthTestedAll = 0;
  UINT64                SubRangeLength = 0;
  UINTN                 PassNumber = 0;

  MtUiSetProgressTotal (MtRangesGetTotalSize ());
  MtUiUpdateProgress (0);

  while (TRUE) {
    Status = MtRangesGetNextRange (
              &Key,
              &Start,
              &Length
              );
    if (Status == EFI_NOT_FOUND) {
      return EFI_SUCCESS;
    } else if (EFI_ERROR (Status)) {
      return Status;
    }

    LengthTested = 0;
    while (LengthTested < Length) {
      SubRangeLength = MIN (SIZE_1MB, Length - LengthTested);

      Status = MtRangesLockRange (Start, SubRangeLength);
      if (EFI_ERROR (Status)) {
        continue;
      }

      for (PassNumber = 0; PassNumber < Test->PassCount; PassNumber++) {
        Test->RangeTest (Start, SubRangeLength, PassNumber, Test->Context);
      }

      MtRangesUnlockRange (Start, SubRangeLength);

      Start += SubRangeLength;
      LengthTested += SubRangeLength;
      LengthTestedAll += SubRangeLength;
      MtUiUpdateProgress (LengthTestedAll);

      if (mAbortTesting) {
        return EFI_ABORTED;
      }
    }
  }
}

/**
  Install memory range test method.

  @param[in]  Name                  The name of memory range test.
  @param[in]  TestRangeFunction     Memory test process.
  @param[in]  NumberOfPasses        Number of pass.
  @param[in]  Context               Test data.

  @retval  EFI_SUCCESS              Operation complete.
  @retval  Others                   Operation failed.

**/
EFI_STATUS
EFIAPI
MtSupportInstallMemoryRangeTest (
  IN  CHAR16                        *Name,
  IN  TEST_MEM_RANGE                TestRangeFunction,
  IN  UINTN                         NumberOfPasses,
  IN  VOID                          *Context
  )
{
  EFI_STATUS          Status = EFI_ABORTED;
  MEM_RANGE_TEST_DATA *NewInstance = NULL;

  NewInstance = (MEM_RANGE_TEST_DATA *)(AllocatePool (sizeof (MEM_RANGE_TEST_DATA)));
  if (NULL == NewInstance) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    return EFI_OUT_OF_RESOURCES;
  }

  NewInstance->RangeTest = TestRangeFunction;
  NewInstance->PassCount = NumberOfPasses;
  NewInstance->Context   = Context;

  Status = MtSupportInstallMemoryTest (
            Name,
            RunMemoryRangeTest,
            (VOID*) NewInstance
            );
  if (EFI_ERROR (Status)) {
    FreePool (NewInstance);
    return Status;
  }

  return Status;
}

/**
  Install memory test method.

  @param[in]  Name                  The name of memory range test.
  @param[in]  TestRangeFunction     Memory test process.
  @param[in]  Context               Test data.

  @retval  EFI_SUCCESS              Operation complete.
  @retval  Others                   Operation failed.

**/
EFI_STATUS
EFIAPI
MtSupportInstallMemoryTest (
  IN  CHAR16                        *Name,
  IN  RUN_MEM_TEST                  MemTestFunction,
  IN  VOID                          *Context
  )
{
  MEM_TEST_INSTANCE *NewTests = NULL;
  UINTN             NewMaxTests = 0;

  //
  // If no space for new test, reallocate space for all tests and
  // copy the old tests to new-allocated spcace.
  //
  if (mTestCount >= mMaxTestCount) {
    NewMaxTests = mMaxTestCount + 32;
    NewTests = (MEM_TEST_INSTANCE*)(AllocatePool (sizeof (MEM_TEST_INSTANCE) * NewMaxTests));
    if (NULL == NewTests) {
      DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
      return EFI_OUT_OF_RESOURCES;
    }

    if ((mTests != NULL) && (mMaxTestCount != 0)) {
      CopyMem (NewTests, mTests, sizeof (MEM_TEST_INSTANCE) * mMaxTestCount);
      FreePool (mTests);
    }

    mMaxTestCount = NewMaxTests;
    mTests = NewTests;
  }

  mTests[mTestCount].Name = Name;
  mTests[mTestCount].RunMemTest = MemTestFunction;
  mTests[mTestCount].Context = Context;
  mTestCount++;

  return EFI_SUCCESS;
}

/**
  Run all memory test.
  TODO: Only use BSP for memory test now, we in fact can use BSP and All
  APs to do the memory tests.

  @param  NA

  @retval  EFI_SUCCESS              Memory test done.
  @retval  Other                    Error happened.

**/
EFI_STATUS
EFIAPI
MtSupportRunAllTests (
  VOID
  )
{
  EFI_STATUS  Status;
  EFI_STATUS  ReturnStatus;
  UINTN       Loop;

  ReturnStatus = EFI_SUCCESS;
  for (Loop = 0; Loop < mTestCount; Loop++) {
    MtUiPrintTestName (mTests[Loop].Name);
    Status = mTests[Loop].RunMemTest (mTests[Loop].Context);
    if (mAbortTesting) {
      MtUiPrint (L"Testing was aborted...\n");
      return EFI_ABORTED;
    }
    if (EFI_ERROR (Status)) {
      ReturnStatus = Status;
    }
  }

  return ReturnStatus;
}

/**
  Executes a WBINVD instruction.

  @param  NA

  @retval  EFI_SUCCESS              Memory test done.
  @retval  Other                    Error happened.

**/
VOID
EFIAPI
MtWbinvd (
  VOID
  )
{
  AsmWbinvd ();
}

/**
  Abort memory test.

  @param  NA

  @retval  NA

**/
VOID
EFIAPI
MtSupportAbortTesting (
  VOID
  )
{
  mAbortTesting = TRUE;
}

/**
  Memory Test Constructor.

  @param  NA

  @retval  EFI_SUCCESS              Always return EFI_SUCCESS.

**/
EFI_STATUS
EFIAPI
MemTestSupportLibConstructor (
  )
{
  return EFI_SUCCESS;
}
