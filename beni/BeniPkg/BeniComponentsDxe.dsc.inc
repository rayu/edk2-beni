##
#  @Package     : BeniPkg
#  @FileName    : BeniComponentsDxe.dsc.inc
#  @Date        : 20230220
#  @Author      : Jiangwei
#  @Version     : 1.0
#  @Description :
#    This file contains components for learning BIOS.
#
#  @History:
#    20230220: Initialize.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

#
# Applications.
#
BeniPkg/App/AppDevPath/AppDevPath.inf
BeniPkg/App/HelloWorldApp/HelloWorldApp.inf
BeniPkg/App/MemTest/MemTest.inf {
  <LibraryClasses>
    MemTestRangesLib|BeniPkg/Library/RangesLib/RangesLib.inf
    MemTestSupportLib|BeniPkg/Library/SupportLib/SupportLib.inf
    MemTestUiLib|BeniPkg/Library/PlainUiLib/UiLib.inf
    #
    # Add all test methods here:
    #
    NULL|BeniPkg/Library/MemTestLib/Patterns/Patterns.inf
    NULL|BeniPkg/Library/MemTestLib/BitShift/BitShift.inf
    NULL|BeniPkg/Library/MemTestLib/Address/Address.inf
}
BeniPkg/App/EventTestApp/EventTestApp.inf

#
# Modules.
#
BeniPkg/Dxe/DxeDriverInBds/DxeDriverInBds.inf
BeniPkg/Dxe/EventTestDxe/EventTestDxe.inf
!if $(BENI_EXT4_SUPPORT) == TRUE
  BeniPkg/Dxe/Ext4Dxe/Ext4Dxe.inf {
      <PcdsFixedAtBuild>
        gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000007
      <LibraryClasses>
        Ucs2Utf8Lib|RedfishPkg/Library/BaseUcs2Utf8Lib/BaseUcs2Utf8Lib.inf
    }
!else
  BeniPkg/Dxe/Ramdisk/Ramdisk.inf
!endif
BeniPkg/Dxe/GetAppFromFv/GetAppFromFv.inf
!if $(INTEL_NIC_SOURCE_CODE) == TRUE
  BeniPkg/Dxe/GigUndiDxe/GigUndiDxe.inf
!endif
BeniPkg/Dxe/GlobalDataInstall/GlobalDataInstall.inf
BeniPkg/Dxe/HelloWorldDxe/HelloWorldDxe.inf
BeniPkg/Dxe/MemoryAllocationTest/MemoryAllocationTest.inf
BeniPkg/Dxe/NullDxeDriverOne/NullDxeDriverOne.inf
BeniPkg/Dxe/NullDxeDriverTwo/NullDxeDriverTwo.inf
BeniPkg/Dxe/PcdTestDriver/PcdTestDriver.inf
BeniPkg/Dxe/ProtocolConsumer/ProtocolConsumer.inf
BeniPkg/Dxe/ProtocolServer/ProtocolServer.inf
BeniPkg/Dxe/HobConsumer/HobConsumer.inf

#
# Redfish modules.
#
EmulatorPkg/Application/RedfishPlatformConfig/RedfishPlatformConfig.inf
!include RedfishPkg/Redfish.dsc.inc
!include RedfishClientPkg/RedfishClient.dsc.inc

#
# Shell commands.
#
BeniPkg/DynamicCommand/DiskDynamicCommand/DiskDynamicCommand.inf {
  <PcdsFixedAtBuild>
    gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE
}
BeniPkg/DynamicCommand/SetupDynamicCommand/SetupDynamicCommand.inf {
  <PcdsFixedAtBuild>
    gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE
}
BeniPkg/DynamicCommand/VarDynamicCommand/VarDynamicCommand.inf {
  <PcdsFixedAtBuild>
    gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE
}
BeniPkg/DynamicCommand/TestDynamicCommand/TestDynamicCommand.inf {
  <PcdsFixedAtBuild>
    gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE
}
BeniPkg/DynamicCommand/ExecuteShellAppCommand/ExecuteShellAppCommand.inf {
  <PcdsFixedAtBuild>
    gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE
}

