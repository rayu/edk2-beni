/**
*  @Package     : BeniPkg
*  @FileName    : EventTestApp.c
*  @Date        : 20230304
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This is an application to test event.
*
*  @History:
*    20230304: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include  <Uefi.h>

#include  <Library/UefiLib.h>
#include  <Library/ShellCEntryLib.h>
#include  <Library/UefiBootServicesTableLib.h>
#include  <Library/DebugLib.h>

#define BENI_EVENT_TIMER_INTERVAL 10000000  // 1s

/**
  Notify the callback function when an event is triggered.

  @param[in]  Event                 Indicates the event that invoke this function.
  @param[in]  Context               Indicates the calling context.

  @retval  NA

**/
VOID
EFIAPI
EventFunc1 (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  UINTN Count = *(UINTN *)Context;

  Print (L"Count: %d\n", Count);
  Count++;
  *(UINTN *)Context = Count;

  if (Count > 3) {
    gBS->CloseEvent (Event);
  }
}

/**
  Event test 1.

  @param  NA

  @retval  NA

**/
VOID
EventTest1 (
  VOID
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  EFI_EVENT   Event = NULL;
  UINTN       Count = 0;

  Status = gBS->CreateEvent (
                  EVT_TIMER | EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  EventFunc1,
                  &Count,
                  &Event
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->SetTimer (
                    Event,
                    TimerPeriodic,
                    BENI_EVENT_TIMER_INTERVAL
                    );
  }

  //
  // Make sure this application is stilling living before event ends.
  //
  gBS->Stall (1000 * 1000 * 6); // 6s
}

/**
  Notify the callback function when an event is triggered.

  @param[in]  Event                 Indicates the event that invoke this function.
  @param[in]  Context               Indicates the calling context.

  @retval  NA

**/
VOID
EFIAPI
EventFunc2 (
  IN  EFI_EVENT                     Event,
  IN  VOID                          *Context
  )
{
  Print (L"Hello World!\n");
  gBS->CloseEvent (Event);
}

/**
  Event test 1.

  @param  NA

  @retval  NA

**/
VOID
EventTest2 (
  VOID
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  EFI_EVENT   Event = NULL;

  Status = gBS->CreateEvent (
                  EVT_NOTIFY_SIGNAL,
                  TPL_CALLBACK,
                  EventFunc2,
                  NULL,
                  &Event
                  );

  Print (L"Waiting to signal event ...\n");
  gBS->Stall (1000 * 1000 * 1);
  gBS->SignalEvent (Event);
}

/**
  Event test 3.

  @param  NA

  @retval  TRUE                     ESC pressed.
  @retval  FALSE                    Esc not pressed

**/
VOID
EventTest3 (
  VOID
  )
{
  EFI_STATUS    Status;
  EFI_INPUT_KEY Key;

  Print (L"Press Esc to quit ...\n");
  do {
    Status = gBS->CheckEvent (gST->ConIn->WaitForKey);
    if (!EFI_ERROR (Status)) {
      Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);
      if (Key.ScanCode == SCAN_ESC) {
        break;
      }
    }
  } while (TRUE);
  Print (L"Esc pressed, goodbye ~\n");
}

/**
  Event test 4.

  @param  NA

  @retval  TRUE                     ESC pressed.
  @retval  FALSE                    Esc not pressed

**/
VOID
EventTest4 (
  VOID
  )
{
  EFI_STATUS  Status = EFI_ABORTED;
  EFI_EVENT   WaitEvt = NULL;
  UINTN       Count = 0;

  Status = gBS->CreateEvent (
                  EVT_TIMER,
                  TPL_CALLBACK,
                  NULL,
                  NULL,
                  &WaitEvt
                  );
  if (!EFI_ERROR (Status)) {
    Status = gBS->SetTimer (
                    WaitEvt,
                    TimerRelative,
                    EFI_TIMER_PERIOD_SECONDS (5)
                    );
  }
  if (EFI_ERROR (Status)) {
    return;
  }

  while (EFI_ERROR (gBS->CheckEvent (WaitEvt))) {
    Print (L"Waiting %d sencond ...\n", ++Count);
    gBS->Stall (1000 * 1000 * 1);
  }

  gBS->SetTimer (WaitEvt, TimerCancel, 0);
  gBS->CloseEvent (WaitEvt);

  return;
}

/**
  The main entry of the application.

  @retval  0                        The application exited normally.
  @retval  Other                    An error occurred.

**/
INTN
EFIAPI
ShellAppMain (
  IN  UINTN                         Argc,
  IN  CHAR16                        **Argv
  )
{
  // EventTest1 ();
  // EventTest2 ();
  // EventTest3 ();
  EventTest4 ();

  return 0;
}
