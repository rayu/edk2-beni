/**
*  @Package     : BeniPkg
*  @FileName    : Exec.c
*  @Date        : 20221210
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is used to execute Shell application.
*
*  @History:
*    20221210: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Exec.h"

STATIC CONST SHELL_PARAM_ITEM ParamList[] = {
  {NULL , TypeMax }
  };

/**
  Execute Shell application.

  @param[IN]  AppName               The application name.

  @return  NA

**/
VOID
Exec (
  IN  CONST CHAR16                  *AppName
  )
{
  EFI_STATUS                Status = EFI_ABORTED;
  EFI_DEVICE_PATH_PROTOCOL  *DevPath = NULL;
  CHAR16                    *Str = NULL;

  DevPath = gEfiShellProtocol->GetDevicePathFromFilePath (AppName);
  if (NULL == DevPath) {
    DEBUG ((EFI_D_ERROR, "Device path not found!\n"));
    return;
  } else {
    Str = ConvertDevicePathToText (DevPath, TRUE, FALSE);
    if (Str) {
      DEBUG ((EFI_D_ERROR, "DevPath: %s\n", Str));
      FreePool (Str);
    }
  }

  Status = gEfiShellProtocol->Execute (&gImageHandle, (CHAR16 *)AppName, NULL, NULL);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "Execute failed. - %r\n", Status));
  }

  return;
}

/**
  Function for 'exec' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunExec (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  SHELL_STATUS  ShellStatus = SHELL_INVALID_PARAMETER;
  EFI_STATUS    Status = EFI_ABORTED;
  LIST_ENTRY    *CheckPackage = NULL;
  CHAR16        *ProblemParam = NULL;
  UINTN         ParamCount = 0;
  CONST CHAR16  *AppName = NULL;

  //
  // Initialize the Shell library (we must be in non-auto-init...).
  //
  Status = ShellInitialize ();
  if (EFI_ERROR (Status)) {
    return SHELL_ABORTED;
  }

  //
  // Parse the command line.
  //
  Status = ShellCommandLineParse (ParamList, &CheckPackage, &ProblemParam, TRUE);
  if (EFI_ERROR (Status)) {
    if ((Status == EFI_VOLUME_CORRUPTED) && (ProblemParam != NULL) ) {
      ShellPrintHiiEx (
        -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
        mExecHiiHandle, L"exec", ProblemParam
        );
      FreePool (ProblemParam);
    }
    goto DONE;
  }

  //
  // Check the number of parameters
  //
  ParamCount = ShellCommandLineGetCount (CheckPackage);
  if (ParamCount < 2) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_TOO_FEW),
      mExecHiiHandle, L"exec"
      );
    goto DONE;
  }
  if (ParamCount > 2) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_TOO_MANY),
      mExecHiiHandle, L"exec"
      );
    goto DONE;
  }

  AppName = ShellCommandLineGetRawValue (CheckPackage, 1);
  if (!AppName) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
      mExecHiiHandle, L"exec"
      );
    goto DONE;
  }

  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_EXEC_START), mExecHiiHandle);
  Exec (AppName);
  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_EXEC_END), mExecHiiHandle);

DONE:

  if ((ShellStatus != SHELL_SUCCESS) && (EFI_ERROR (Status))) {
    ShellStatus = Status & ~MAX_BIT;
  }

  return ShellStatus;
}
