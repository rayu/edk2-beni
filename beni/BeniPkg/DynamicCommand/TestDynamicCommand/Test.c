/**
*  @Package     : BeniPkg
*  @FileName    : Test.c
*  @Date        : 20220313
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    This command is for test only.
*
*  @History:
*    20220313: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include "Test.h"

STATIC CONST SHELL_PARAM_ITEM ParamList[] = {
  {L"asm",  TypeFlag},
  {L"json", TypeFlag},
  {NULL,    TypeMax }
};

/**
  Test assembly code.

  @param  NA

  @return  NA

**/
STATIC
VOID
TestAsm (
  VOID
  )
{
  BeniAsmNop ();
  // BeniAsmLoopInfi ();
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
  BeniAsmSerialIo ('A');
}

/**
  Test JSON code.

  @param[in]  Param                 The JOSN file name.

  @return  NA

**/
STATIC
VOID
TestJson (
  IN  CONST CHAR16                  *Param
  )
{
  EFI_STATUS            Status = EFI_ABORTED;
  CHAR16                *FileName = NULL;
  SHELL_FILE_HANDLE     FileHandle = NULL;
  UINT64                FileSize = 0;
  UINT8                 *FileData = NULL;
  EDKII_JSON_VALUE      Json = NULL;
  EDKII_JSON_VALUE      JsonData = NULL;
  EDKII_JSON_VALUE      NewData = NULL;
  EDKII_JSON_ERROR      JsonError;
  CHAR8                 *NewJson = NULL;

  FileName = ShellFindFilePath (Param);
  if (NULL == FileName) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  Print (L"Open %s ...\r\n", FileName);

  Status = ShellOpenFileByName (
            FileName,
            &FileHandle,
            EFI_FILE_MODE_READ,
            0
            );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  Status = gEfiShellProtocol->GetFileSize (FileHandle, &FileSize);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }
  Print (L"File size %d ...\r\n", FileSize);

  FileData = AllocateZeroPool (FileSize);
  if (NULL == FileData) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Out of memory\n", __FUNCTION__, __LINE__));
    goto DONE;
  }

  Status = ShellReadFile (FileHandle, &FileSize, FileData);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    return;
  }

  Status = ShellCloseFile (&FileHandle);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  } else {
    FileHandle = NULL;
  }

  Print (L"File data: [%a]\r\n", FileData);
  BeniDumpHex (2, 0, FileSize, FileData);

  Json = JsonLoadBuffer (FileData, FileSize, 0x4, &JsonError);
  if (NULL == Json) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed.\n", __FUNCTION__, __LINE__));
    goto DONE;
  }

  Print (L"JsonValueIsObject: %d\r\n", JsonValueIsObject (Json));
  Print (L"Json1: %a\r\n", JsonDumpString (Json, EDKII_JSON_ENSURE_ASCII));

  JsonData = JsonObjectGetValue (Json, "name");
  if (JsonValueIsString (JsonData)) {
    Print (L"Name: %a\r\n", JsonValueGetAsciiString (JsonData));
  }

  // Create new JSON object for string.
  NewData = JsonValueInitAsciiString ("JIANGWEI");
  if (NULL == NewData) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  // Set JSON value.
  Status = JsonObjectSetValue (Json, "name", NewData);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  NewJson = JsonDumpString (Json, EDKII_JSON_ENSURE_ASCII);
  Print (L"Json2: %a\r\n", NewJson);

  Status = ShellOpenFileByName (
            FileName,
            &FileHandle,
            EFI_FILE_MODE_WRITE | EFI_FILE_MODE_READ,
            0
            );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

  SHELL_FREE_NON_NULL (FileData);
  FileSize = AsciiStrLen (NewJson);
  FileData = AllocateZeroPool (FileSize + 2);
  CopyMem (FileData, JsonDumpString (Json, EDKII_JSON_ENSURE_ASCII), FileSize);
  FileData[FileSize] = 0xD;
  FileData[FileSize + 1] = 0xA;
  FileSize = FileSize + 2;
  BeniDumpHex (2, 0, FileSize, FileData);
  Status = ShellWriteFile (FileHandle, &FileSize, FileData);
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
    goto DONE;
  }

DONE:

  SHELL_FREE_NON_NULL (FileData);

  return;
}

/**
  Function for 'test' command.

  @param[in]  ImageHandle           The image handle.
  @param[in]  SystemTable           The system table.

  @retval  SHELL_SUCCESS            Command completed successfully.
  @retval  SHELL_INVALID_PARAMETER  Command usage error.
  @retval  SHELL_ABORTED            The user aborts the operation.
  @retval  Value                    Unknown error.

**/
SHELL_STATUS
RunTest (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  SHELL_STATUS  ShellStatus = SHELL_INVALID_PARAMETER;
  EFI_STATUS    Status = EFI_ABORTED;
  LIST_ENTRY    *CheckPackage = NULL;
  CHAR16        *ProblemParam = NULL;
  CONST CHAR16  *Param = NULL;
  UINTN         ParamCount = 0;

  //
  // Initialize the Shell library (we must be in non-auto-init...).
  //
  Status = ShellInitialize ();
  if (EFI_ERROR (Status)) {
    return SHELL_ABORTED;
  }

  //
  // Parse the command line.
  //
  Status = ShellCommandLineParse (ParamList, &CheckPackage, &ProblemParam, TRUE);
  if (EFI_ERROR (Status)) {
    if ((Status == EFI_VOLUME_CORRUPTED) && (ProblemParam != NULL) ) {
      ShellPrintHiiEx (
        -1, -1, NULL, STRING_TOKEN (STR_GEN_PROBLEM),
        mTestHiiHandle, L"test", ProblemParam
        );
      FreePool (ProblemParam);
    }
    goto DONE;
  }

  //
  // Check the number of parameters
  //
  ParamCount = ShellCommandLineGetCount (CheckPackage);
  if (ParamCount > 2) {
    ShellPrintHiiEx (
      -1, -1, NULL, STRING_TOKEN (STR_GEN_TOO_MANY),
      mTestHiiHandle, L"test"
      );
    goto DONE;
  }

  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TEST_START), mTestHiiHandle);
  if (ShellCommandLineGetFlag (CheckPackage, L"asm")) {
    TestAsm ();
  }
  if (ShellCommandLineGetFlag (CheckPackage, L"json")) {
    Param = ShellCommandLineGetRawValue (CheckPackage, 1);
    if (NULL == Param) {
      Param = L"example.json";
    }
    TestJson (Param);
  }
  ShellPrintHiiEx (-1, -1, NULL, STRING_TOKEN (STR_TEST_END), mTestHiiHandle);

DONE:

  if ((ShellStatus != SHELL_SUCCESS) && (EFI_ERROR (Status))) {
    ShellStatus = Status & ~MAX_BIT;
  }

  return ShellStatus;
}
