##
#  @Package     : BeniPkg
#  @FileName    : BeniLibs.dsc.inc
#  @Date        : 20230225
#  @Author      : Jiangwei
#  @Version     : 1.0
#  @Description :
#    This file contains components for learning BIOS.
#
#  @History:
#    20230225: Initialize.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

[LibraryClasses]
  BeniAsmLib|BeniPkg/Library/AsmLib/AsmLib.inf
  BeniTimeLib|BeniPkg/Library/TimeLib/TimeLib.inf
  BeniGlobalDataTestLib|BeniPkg/Library/GlobalDataTestLib/GlobalDataTestLib.inf
  BeniHiiLib|BeniPkg/Library/HiiLib/HiiLib.inf
  BeniDebugInfoLib|BeniPkg/Library/DebugInfoLib/DebugInfoLib.inf
!if $(REDFISH_ENABLE) == TRUE
  RedfishPlatformHostInterfaceLib|EmulatorPkg/Library/RedfishPlatformHostInterfaceLib/RedfishPlatformHostInterfaceLib.inf
  RedfishPlatformCredentialLib|EmulatorPkg/Library/RedfishPlatformCredentialLib/RedfishPlatformCredentialLib.inf
!endif

[LibraryClasses.common.PEIM]

[LibraryClasses.common.DXE_DRIVER]
