/**
*  @Package     : BeniPkg
*  @FileName    : DxeDriverInBds.c
*  @Date        : 20211004
*  @Author      : Jiangwei
*  @Version     : 1.0
*  @Description :
*    We can have the DXE driver executed in BDS stage and this driver is an example.
*
*  @History:
*    20211004: Initialize.
*
*  This program and the accompanying materials
*  are licensed and made available under the terms and conditions of the BSD License
*  which accompanies this distribution. The full text of the license may be found at
*  http://opensource.org/licenses/bsd-license.php
*
*  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
*  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
**/

#include <Uefi.h>

#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DebugLib.h>

#include <Protocol/PciIo.h>

/**
  Main entry of the driver.

  @param[in]  ImageHandle           Image handle this driver.
  @param[in]  SystemTable           Pointer to the System Table.

  @retval  EFI_SUCCESS              Driver executed successfully.
  @retval  Others                   Error happened.

**/
EFI_STATUS
EFIAPI
DxeDriverInBdsEntry (
  IN  EFI_HANDLE                    ImageHandle,
  IN  EFI_SYSTEM_TABLE              *SystemTable
  )
{
  EFI_STATUS          Status = EFI_ABORTED;
  EFI_PCI_IO_PROTOCOL *PciIo = NULL;
  EFI_HANDLE          *Handles = NULL;
  UINTN               HandleCount = 0;
  UINTN               Index = 0;
  UINT16              VendorId = 0;
  UINT16              DeviceId = 0;

  Status = gBS->LocateHandleBuffer (
                  ByProtocol,
                  &gEfiPciIoProtocolGuid,
                  NULL,
                  &HandleCount,
                  &Handles
                  );
  if (EFI_ERROR (Status)) {
    DEBUG ((EFI_D_ERROR, "[%a][%d] Failed. - %r\n", __FUNCTION__, __LINE__, Status));
  }

  for (Index = 0; Index < HandleCount; Index++) {
    Status = gBS->HandleProtocol (
                    Handles[Index],
                    &gEfiPciIoProtocolGuid,
                    (VOID **)&PciIo
                    );
    if (!EFI_ERROR (Status)) {
      Status = PciIo->Pci.Read (
                        PciIo,
                        EfiPciIoWidthUint16,
                        0x00,
                        1,
                        &VendorId
                        );
      if (EFI_ERROR (Status)) {
        continue;
      }

      Status = PciIo->Pci.Read (
                        PciIo,
                        EfiPciIoWidthUint16,
                        0x02,
                        1,
                        &DeviceId
                        );
      if (EFI_ERROR (Status)) {
        continue;
      }

      DEBUG ((EFI_D_ERROR, "[%a] VendorId: 0x%04x, DeviceId: 0x%04x\n",
        __FUNCTION__, VendorId, DeviceId));
    }
  }

  if (NULL != Handles) {
    FreePool (Handles);
    Handles = NULL;
  }

  return EFI_SUCCESS;
}
