##
#  @Package     : BeniPkg
#  @FileName    : BeniDefines.dsc.inc
#  @Date        : 20230220
#  @Author      : Jiangwei
#  @Version     : 1.0
#  @Description :
#    This file contains components for learning BIOS.
#
#  @History:
#    20230220: Initialize.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

#
# Serial output when in POST.
#
DEFINE DEBUG_ON_SERIAL_PORT = TRUE
#
# If EXT4 not used, ramdisk will be installed.
#
DEFINE BENI_EXT4_SUPPORT = FALSE
#
# Where the shell applications reside in.
# This should be relative to MACROs in build.cmd.
#
DEFINE COMPILE_DIR = DEBUG_VS2015x86
#
# Check if Intel GigUndi is complied.
#
DEFINE INTEL_NIC_SOURCE_CODE = FALSE
#
# Add Redfish modules.
#
DEFINE REDFISH_ENABLE = TRUE
DEFINE REDFISH_CLIENT = TRUE
