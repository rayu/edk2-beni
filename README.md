# 简述

用于学习和验证UEFI BIOS。

# 代码说明

除了本仓库包含的代码，构建UEFI BIOS还需要如下的代码，可以在开源网站下载到：

* edk2：[edk2: https://github.com/tianocore/edk2.git (gitee.com)](https://gitee.com/jiangwei0512/edk2)
* brotli：[brotli: https://github.com/google/brotli (gitee.com)](https://gitee.com/jiangwei0512/brotli)
* oniguruma：[oniguruma: https://github.com/kkos/oniguruma.git (gitee.com)](https://gitee.com/jiangwei0512/oniguruma)
* openssl：[openssl: https://github.com/openssl/openssl.git (gitee.com)](https://gitee.com/jiangwei0512/openssl)
* jansson：[jansson: https://github.com/akheron/jansson.git (gitee.com)](https://gitee.com/jiangwei0512/jansson)
* edk2-staging：[edk2-staging: 来自https://github.com/tianocore/edk2-staging.git (gitee.com)](https://gitee.com/jiangwei0512/edk2-staging)，使用分支edk2-redfish-client，注意对应的代码目录是staging，因为使用edk-staging会报错。

注意代码需要注意版本控制，edk2不同的分支依赖于其它代码的不同提交版本。

build.cmd脚本中已经包含了下载代码和切换分支的操作，不用自己去下载。

# 依赖软件

推荐安装以下软件：

* Visual Studio，使用默认安装路径，否则编译会报错。
* git，本仓库不包含构建UEFI BIOS需要的所有代码，需要额外的下载，所以需要git工具。
* ASL，放在C盘根目录，用于编译ACPI代码。
* Nasm，放在C盘根目录，用来编译汇编代码。
* Visual Studio Code，代码编辑工具，非必要，推荐。
* Typora，Markdown程序，非必要，你现在看的文档就是用这个程序完成的。
* QEMU，用于运行本项目构建出来的BIOS。

注意不同的edk2分支代码需要的软件版本可能不同，推荐下载最新的版本。

# 使用方法

1. 点击build.cmd下载代码，下载完成之后会自动开始构建UEFI BIOS。



